#!/usr/bin/env python3
import hw
import lf
from ev3dev2 import motor
import math
import time
from ev3dev2.button import Button

DIST_THRESH = 12
TURN_SP = 100

def avoid_obstacle():
    hw.cs.mode = hw.cs.MODE_COL_COLOR
    hw.mdr.on_for_degrees(motor.SpeedRPM(-TURN_SP), 9.9 * 2 * math.pi / 360 * 37 / (5.6 * math.pi) * 360)
    hw.mdr.on(motor.SpeedDPS(500))
    hw.mdl.on(motor.SpeedDPS(500))
    time.sleep(1) # TODO: find better time
    hw.mdl.stop(stop_action='brake')
    hw.mdr.stop(stop_action='brake')
    hw.mdr.on_for_degrees(motor.SpeedRPM(TURN_SP), 9.9 * 2 * math.pi / 360 * 90 / (5.6 * math.pi) * 360)
    hw.mdr.on(motor.SpeedDPS(500))
    hw.mdl.on(motor.SpeedDPS(500))
    done = False
    time.sleep(0.5) # TODO: find better time
    while hw.ls.reflected_light_intensity > 40:
        time.sleep(0.05)
    hw.mdl.stop(stop_action='coast')
    hw.mdr.stop(stop_action='coast')
    hw.mdr.on_for_degrees(motor.SpeedRPM(-TURN_SP), 9.9 * 2 * math.pi / 360 * 37 / (5.6 * math.pi) * 360)

def linerm():
    done = False

    b = Button()

    green_counter = 0
    def exit_cond():
        nonlocal done
        nonlocal green_counter
        dist = hw.us.distance_centimeters
        if dist < DIST_THRESH:
            return True
        else:
            if hw.cs.color == 5:
                green_counter += 1
            else:
                green_counter = 0
            if green_counter >= 3 or b.any():
                done = True
                return True
            else:
                return False
    while not done:
        lf.lf(exit_cond)
        if not done:
            avoid_obstacle()

if __name__ == '__main__':
    linerm()

