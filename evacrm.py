#!/usr/bin/env python3
import hw
import hal
from ev3dev2 import motor
import time

BASE_SPEED = 20

def clamp(l, h, v):
    return min(max(v, l), h)

def wall_align(tm=1):
    hw.mTank.on_for_seconds(motor.SpeedDPS(-400), motor.SpeedDPS(-400), tm)
    hal.move_dist(5, motor.SpeedDPS(400))

def check_bomb():
    return hw.ir.value(0) != 0

def grab_bomb(ftime):
    start = time.time()
    prev_time = start
    past_steers = []
    hw.mdr.run_direct(duty_cycle_sp=BASE_SPEED)
    hw.mdl.run_direct(duty_cycle_sp=BASE_SPEED)
    while time.time() - start < ftime:
        steer = (hw.ir.value(0) - 5) * 10
        hw.mdr.duty_cycle_sp = clamp(-100, 100, (BASE_SPEED + steer))
        hw.mdl.duty_cycle_sp = clamp(-100, 100, (BASE_SPEED - steer))
        past_steers.append((steer, time.time() - prev_time))
        prev_time = time.time()
    hw.mdl.stop(stop_action='coast')
    hw.mdr.stop(stop_action='coast')
    hal.claw_close()
    end_steer = sum([x for x, t in past_steers]) / len(past_steers) * 10
    hw.mTank.on_for_seconds(motor.SpeedDPS(-200 + end_steer), motor.SpeedDPS(-200 - end_steer), ftime)
    wall_align()

def bomb_rm1():
    grab_bomb(5)
    hal.turn_degs(-90, 200, motor.SpeedDPS)
    wall_align()
    hal.move_dist(30, motor.SpeedDPS(200))
    hal.turn_degs(-90, 200, motor.SpeedDPS)
    wall_align()
    hal.move_dist(15, motor.SpeedDPS(400))

def bomb_rm2():
    grab_bomb(5)
    hal.turn_degs(-90, 200, motor.SpeedDPS)
    wall_align(3)
    hal.turn_degs(90, 200, motor.SpeedDPS)
    hal.move_dist(30, motor.SpeedDPS(400))
    hal.turn_degs(90, 200, motor.SpeedDPS)
    wall_align()
    hal.move_dist(15, motor.SpeedDPS(400))
    pass

def bomb_rm3():
    grab_bomb()
    hal.turn_degs(90, 200, motor.SpeedDPS)
    wall_align()
    hal.move_dist(60, motor.SpeedDPS(200))
    hal.turn_degs(90, 200, motor.SpeedDPS)
    wall_align()
    hal.move_dist(15, motor.SpeedDPS(200))

def evac_room():
    hal.claw_open()
    hw.mTank.on(motor.SpeedDPS(400), motor.SpeedDPS(400))
    while hw.us.distance_centimeters >= 17:
        time.sleep(0.05)
    hal.turn_degs(180, 400, motor.SpeedDPS)
    wall_align()
    hal.turn_degs(90, 400, motor.SpeedDPS)
    wall_align(3)
    hal.turn_degs(90, 400, motor.SpeedDPS)
    hal.move_dist(15, motor.SpeedDPS(400))
    if check_bomb():
        bomb_rm3()
        return
    wall_align(2)
    hal.turn_degs(-90, 400, motor.SpeedDPS)
    hal.move_dist(62, motor.SpeedDPS(400))
    hal.turn_degs(90, 400, motor.SpeedDPS)
    wall_align()
    hal.move_dist(35, motor.SpeedDPS(400))
    hal.turn_degs(90, 400, motor.SpeedDPS)
    wall_align()
    if check_bomb():
        bomb_rm2()
        return
    hal.turn_degs(-90, 400, motor.SpeedDPS)
    wall_align(3)
    hal.turn_degs(90, 400, motor.SpeedDPS)
    wall_align(3)
    hal.turn_degs(-90, 400, motor.SpeedDPS)
    if check_bomb():
        bomb_rm1()
        return
    # ???

if __name__ == '__main__':
    evac_room()
