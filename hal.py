import hw
import mutil
from ev3dev2 import motor
from time import sleep
import math

WHEEL_DIA = 5.6

def wait_stop(m):
    m.wait_until('running')
    m.wait_until_not_moving()

def claw_open():
    hw.mf.on_for_seconds(-20, 0.5, brake=False)
    # mutil.on_for_degrees(hw.mf, motor.SpeedRPM(+10), +67.5, stop_action='coast')

def claw_close():
    hw.mf.on(10)
    sleep(2)

def move_dist(dist, speed):
    hw.mTank.on_for_degrees(speed, speed, dist / (WHEEL_DIA * math.pi) * 360)
    sleep(0.1)

def turn_degs(degs, speed_val, speed_cls):
    d = 1 if degs > 0 else -1
    hw.mTank.on_for_degrees(speed_cls(speed_val * d), speed_cls(speed_val * -d),
        9.9 * math.pi / 360 * abs(degs) / (5.6 * math.pi) * 360
    )
    sleep(0.1)
