#!/usr/bin/env python3
import hal
import time

hal.claw_open()
time.sleep(1)
hal.claw_close()
time.sleep(5)
hal.claw_open()
