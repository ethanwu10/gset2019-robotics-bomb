def on_for_degrees(self, speed, degrees, stop_action='coast', block=True):
    """
    Rotate the motor at ``speed`` to ``position``
    ``speed`` can be a percentage or a :class:`ev3dev2.motor.SpeedValue`
    object, enabling use of other units.
    """
    speed_sp = self._speed_native_units(speed)
    self._set_rel_position_degrees_and_speed_sp(degrees, speed_sp)
    self.stop_action = stop_action
    self.run_to_rel_pos()

    if block:
        self.wait_until('running')
        self.wait_until_not_moving()
