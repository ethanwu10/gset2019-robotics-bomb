from ev3dev2 import motor
from ev3dev2 import sensor
from ev3dev2.sensor import lego

_PML = motor.OUTPUT_A
_PMR = motor.OUTPUT_D

mf = motor.MediumMotor(motor.OUTPUT_C)

mdl = motor.LargeMotor(_PML)
mdr = motor.LargeMotor(_PMR)

mTank = motor.MoveTank(_PML, _PMR, motor_class=motor.LargeMotor)

ls = lego.LightSensor()
cs = lego.ColorSensor()
us = lego.UltrasonicSensor()
ir = sensor.Sensor('ev3-ports:in2:i2c8')
