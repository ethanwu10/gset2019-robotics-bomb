import collections

class PID:
    def __init__(self, kp, ki, kd, window_len=10):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.past_err = collections.deque(maxlen=window_len)
        self.i_term = 0

    def update(self, error):
        self.past_err.append(error)

        correction = 0
        correction += error * self.kp
        correction += self.i_term * self.ki

        deriv = 0
        last = None
        for e in self.past_err:
            if last is not None:
                deriv += e - last
            last = e
        try:
            deriv /= (len(self.past_err) - 1)
        except ZeroDivisionError:
            deriv = 0

        correction += deriv * self.kd

        self.i_term += error

        return correction
