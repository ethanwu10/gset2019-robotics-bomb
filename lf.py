#!/usr/bin/env python3
import hw
from ev3dev2.button import Button
import pid

import subprocess

MIDPOINT = 42
BASE_SPEED = 25

kP = 3.5
kI = 0.1
kD = 1.8

speed_correcto = 1.2


MARKER_COL = 4
SNDFILE = '/home/robot/watchyojet.wav'


def clamp(l, h, v):
    return min(max(v, l), h)

def lf(exit_cond):
    hw.ls.mode=hw.ls.MODE_REFLECT
    hw.cs.mode=hw.cs.MODE_COL_COLOR
    hw.mdl.run_direct(duty_cycle_sp=BASE_SPEED)
    hw.mdr.run_direct(duty_cycle_sp=BASE_SPEED)
    hw.mf.on(-5)
    controller = pid.PID(kP, kI, -kD, window_len=2)

    inSegment = False

    sndPlayer = None

    while not exit_cond():
        intensity = hw.ls.reflected_light_intensity
        err = intensity - MIDPOINT
        corr = controller.update(err)
        hw.mdr.duty_cycle_sp = clamp(-100, 100, (1 + speed_correcto**-abs(err))*(BASE_SPEED + corr))
        hw.mdl.duty_cycle_sp = clamp(-100, 100, (1 + speed_correcto**-abs(err))*(BASE_SPEED - corr))

        if hw.cs.color == MARKER_COL:
            if not inSegment:
                inSegment = True
                if sndPlayer is not None:
                    sndPlayer.terminate()
                    sndPlayer.kill()
                sndPlayer = subprocess.Popen(['/usr/bin/aplay', SNDFILE])
        else:
            if inSegment:
                inSegment = False

    hw.mdl.stop(stop_action='coast')
    hw.mdr.stop(stop_action='coast')
    hw.mf.stop(stop_action='coast')

if __name__ == '__main__':
    b = Button()
    def stop_fn():
        return b.any()

    lf(stop_fn)
